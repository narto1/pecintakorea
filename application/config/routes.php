<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Home';
$route['movie/(:any)'] = 'Single/movie/$1';
$route['terpopuler/(:any)'] = 'More/terpopuler/$1';
$route['terpopuler'] = 'More/terpopuler';
$route['terbaru/(:any)'] = 'More/terbaru/$1';
$route['terbaru'] = 'More/terbaru';
$route['ongoing/(:any)'] = 'More/ongoing/$1';
$route['ongoing'] = 'More/ongoing';
$route['genre/(:any)/(:any)'] = 'More/genre/$1/$2';
$route['genre/(:any)'] = 'More/genre/$1/$2';
$route['actor/(:any)/(:any)'] = 'More/actor/$1/$2';
$route['actor/(:any)'] = 'More/actor/$1/$2';
$route['director/(:any)/(:any)'] = 'More/director/$1/$2';
$route['director/(:any)'] = 'More/director/$1/$2';
$route['production/(:any)/(:any)'] = 'More/production/$1/$2';
$route['production/(:any)'] = 'More/production/$1/$2';
$route['country/(:any)/(:any)'] = 'More/country/$1/$2';
$route['country/(:any)'] = 'More/country/$1/$2';
$route['list/(:any)/(:any)'] = 'More/list/$1/$2';
$route['list/(:any)'] = 'More/list/$1/$2';
$route['search/(:any)/(:any)'] = 'More/search/$1/$2';
$route['search/(:any)'] = 'More/search/$1/$2';
$route['search_keyword'] = 'More/search_keyword';
$route['series/(:any)/(:any)'] = 'Single/series/$1/$2';
$route['series/(:any)'] = 'Single/series/$1';
$route['request'] = 'Single/request';
$route['dmca'] = 'Single/dmca';
$route['faq'] = 'Single/faq';
$route['404_override'] = 'Home/redirect_home';
$route['translate_uri_dashes'] = FALSE;
