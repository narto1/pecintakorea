<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_home extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('id')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_big_four($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(4)->get_where('listmovie', $where)->result();
	}

	public function get_popular_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_latest_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('id', 'desc')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_ongoing_post($where = array())
	{
		$where['status'] = 'publish';
		$where['jenis'] = 'series';
		$where['progres'] = 'ongoing';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(12)->get_where('listmovie', $where)->result();
	}

	public function get_featured_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('release_date', 'desc')->limit(12)->get_where('listmovie', $where)->result();
	}

	public function get_movie_imdb($id)
	{
		$api_key = "cf865a5d";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'http://www.omdbapi.com/?i='.$id."&apikey=".$api_key."&plot=full");
		$content = curl_exec($ch);
		curl_close($ch);
		return json_decode($content);
	}
}
?>