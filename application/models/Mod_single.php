<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_single extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_movie_data($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->limit(1)->get_where('listmovie', $where)->row();
	}

	public function get_episode_data($where = array())
	{
		return $this->db->select('*')->limit(1)->get_where('episode', $where)->row();
	}

	public function get_all_episode($where = array())
	{
		return $this->db->select('*')->get_where('episode', $where)->result();
	}

	public function get_related_post($slug)
	{
		$where['status'] = 'publish';
		$where['slug'] = $slug;
		$data_post = $this->db->select('*')->limit(1)->get_where('listmovie',$where)->row();
		$split_genre = explode(',', $data_post->genre);
		for ($i=0; $i < count($split_genre); $i++) { 
			if($i === 0)
			{
				$genre = 'genre LIKE "%'.$split_genre[$i].'%"';
			}else{
				$genre = $genre.' OR genre LIKE "%'.$split_genre[$i].'%"';
			}
		}
		$this->db->where('status', 'publish');
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(8)->get_where('listmovie', $genre)->result();
	}

	public function get_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('id')->get_where('listmovie', $where)->result();
	}

	public function get_big_four($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(4)->get_where('listmovie', $where)->result();
	}

	public function get_popular_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_latest_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('id', 'desc')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_ongoing_post($where = array())
	{
		$where['status'] = 'publish';
		$where['jenis'] = 'series';
		$where['progres'] = 'ongoing';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(12)->get_where('listmovie', $where)->result();
	}

	public function get_featured_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('release_date', 'desc')->limit(12)->get_where('listmovie', $where)->result();
	}

	public function get_movie_imdb($id)
	{
		$api_key = "cf865a5d";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'http://www.omdbapi.com/?i='.$id."&apikey=".$api_key."&plot=full");
		$content = curl_exec($ch);
		curl_close($ch);
		return json_decode($content);
	}

	public function check_movie($slug)
	{
		$where['status'] = 'publish';
		$where['slug'] = $slug;
		$where['jenis'] = 'movie';
		if ($this->db->select('*')->get_where('listmovie', $where)->num_rows() === 0) {
			return redirect('home');
		}
	}
	public function add_viewer($slug)
	{
		$current_view = $this->db->select('*')->limit(1)->get_where('listmovie', array('slug'=>$slug))->row()->viewer;
		$current_view += 1;
		$this->db->set('viewer', $current_view);
		return $this->db->where('slug', $slug)->update('listmovie');
	}

	public function check_series($slug, $eps, $imdbid)
	{
		$where['status'] = 'publish';
		$where['slug'] = $slug;
		$where['jenis'] = 'series';
		if ($this->db->select('*')->get_where('listmovie', $where)->num_rows() === 0 || $this->db->select('*')->get_where('episode', array('movie_imdb_id'=>$imdbid,'episode'=>$eps))->num_rows() === 0) {
			return redirect('home');
		}
	}

	public function send_request_film($email, $judul)
	{
		$data = array('sender' => $email, 'messages' => $judul);
		if ($this->db->insert('request', $data)) {
			$this->session->set_flashdata('success', 'Terima kasih, request anda akan kami proses');
		}
		return redirect('request');
	}

	function where()
	{
		$this->db->where('status', 'publish');
	}
}
?>