<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_more extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_post_order_by($page, $order, $sort)
	{
		$where['status'] = 'publish';
		$this->check_page($page);
		$rowlimit = $page*24;
		$rowlimit -= 24;
		return $this->db->select('*')->order_by($order, $sort)->limit(24,$rowlimit)->get_where('listmovie', $where)->result();
	}

	public function get_post_order_by_like($page, $order, $sort, $like)
	{
		$where['status'] = 'publish';
		$this->check_page($page);
		$rowlimit = $page*24;
		$rowlimit -= 24;
		return $this->db->select('*')->like($like)->order_by($order, $sort)->limit(24,$rowlimit)->get_where('listmovie', $where)->result();
	}

	public function get_post_order_by_where($page, $order, $sort, $where)
	{
		$where['status'] = 'publish';
		$this->check_page($page);
		$rowlimit = $page*24;
		$rowlimit -= 24;
		return $this->db->select('*')->order_by($order, $sort)->limit(24,$rowlimit)->get_where('listmovie', $where)->result();
	}

	public function get_post_where($page, $where = array())
	{
		$where['status'] = 'publish';
		$this->check_page_where($page, $where);
		$rowlimit = $page*24;
		$rowlimit -= 24;
		return $this->db->select('*')->limit(24,$rowlimit)->get_where('listmovie', $where)->result();
	}

	public function get_post_like($page, $like = array())
	{
		$where['status'] = 'publish';
		$this->check_page_like($page, $like);
		$rowlimit = $page*24;
		$rowlimit -= 24;
		return $this->db->select('*')->limit(24,$rowlimit)->like($like)->get_where('listmovie', $where)->result();
	}

	public function get_total_row($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->get_where('listmovie', $where)->num_rows();
	}

	public function get_total_row_like($like = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->like($like)->get('listmovie')->num_rows();
	}

	public function get_popular_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_latest_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('id', 'desc')->limit(24)->get_where('listmovie', $where)->result();
	}

	public function get_ongoing_post($where = array())
	{
		$where['status'] = 'publish';
		$where['jenis'] = 'series';
		$where['progres'] = 'ongoing';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(12)->get_where('listmovie', $where)->result();
	}

	public function get_featured_post($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('release_date', 'desc')->limit(12)->get_where('listmovie', $where)->result();
	}

	public function get_movie_imdb($id)
	{
		$api_key = "cf865a5d";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_URL, 'http://www.omdbapi.com/?i='.$id."&apikey=".$api_key."&plot=full");
		$content = curl_exec($ch);
		curl_close($ch);
		return json_decode($content);
	}

	function check_page_where($page, $where = array())
	{
		$where['status'] = 'publish';
		$total_page = $this->db->select('*')->get_where('listmovie', $where)->num_rows() / 24;
		if ($page > $total_page && $page > 1) {
			return redirect('home');
		}
	}

	function check_page_like($page, $like = array())
	{
		$where['status'] = 'publish';
		$total_page = $this->db->select('*')->like($like)->get_where('listmovie', $where)->num_rows() / 24;
		if ($page > $total_page && $page > 1) {
			return redirect('home');
		}
	}

	function check_page($page)
	{
		$where['status'] = 'publish';
		$total_page = $this->db->select('*')->get_where('listmovie', $where)->num_rows() / 24;
		if ($page > $total_page && $page > 1) {
			return redirect('home');
		}
	}

	public function correct_keyword($keyword){
		$specChars = array(
		    '%21' => '!',    '%22' => '"',
		    '%23' => '#',    '%24' => '$',    '%25' => '%',
		    '%26' => '&',    '%27' => '\'',   '%28' => '(',
		    '%29' => ')',    '%2A' => '*',    '%2B' => '+',
		    '%2C' => ',',    '%2D' => '-',    '%2E' => '.',
		    '%2F' => '/',    '%3A' => ':',    '%3B' => ';',
		    '%3C' => '<',    '%3D' => '=',    '%3E' => '>',
		    '%3F' => '?',    '%40' => '@',    '%5B' => '[',
		    '%5C' => '\\',   '%5D' => ']',    '%5E' => '^',
		    '%5F' => '_',    '%60' => '`',    '%7B' => '{',
		    '%7C' => '|',    '%7D' => '}',    '%7E' => '~',
		    '%E2%80%9A' => ',',  '%20' => ' '
		);

		foreach ($specChars as $k => $v) {
		    $keyword = str_replace($k, $v, $keyword);
		}
		return $keyword;
	}
}
?>