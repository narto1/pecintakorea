<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mod_frontend extends CI_model {
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function get_banner($where = array())
	{
		return $this->db->select('*')->get_where('ads', $where)->result();
	}

	public function get_top_5($where = array())
	{
		$where['status'] = 'publish';
		return $this->db->select('*')->order_by('viewer', 'desc')->limit(5)->get_where('listmovie', $where)->result();
	}

	public function url_image_seo ($slug)
	{
		$que = $this->db->select("*")->get_where("listmovie", array('slug'=>$slug));
		foreach ($que->result() as $key) {
		  return base_url($key->url_image);
		}
	}
}
?>