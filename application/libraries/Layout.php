<?php
class Layout {
	private $CI;

	public function __construct() {
		$this->CI = &get_instance();
	}

	public function view($data = [], $layout = 'frame') {
		$this->CI->load->model('Mod_frontend');
		$data_banner = $this->CI->Mod_frontend->get_banner();
		foreach ($data_banner as $key => $value) {
			$data[$value->kode] = $value;
		}
		if (isset($data['slug'])) {
			$data['url_image_seo'] = $this->CI->Mod_frontend->url_image_seo($data['slug']);
		}else{
			$data['url_image_seo'] = base_url("assets/images/lOGO1.png");
		}
		if (isset($data['meta_keyword'])) {
			$data['meta_keyword'] = str_replace('-', ' ', $data['meta_keyword']).', Pecinta Korea, Pecinta Korea Streaming, Streaming Drama Korea, Drakor, Drama Korea, Nonton Drama Korea, Drama Korea Subindo, Drakor Subindo, nonton drama, cinemaindo, cinema korea, cinema21, layar kaca 21, dunia 21, Subindo, subtitle indonesia, indoxx1, indoxxi';
		}else{
			$data['meta_keyword'] = 'Pecinta Korea, Pecinta Korea Streaming, Streaming Drama Korea, Drakor, Drama Korea, Nonton Drama Korea, Drama Korea Subindo, Drakor Subindo, nonton drama, cinemaindo, cinema korea, cinema21, layar kaca 21, dunia 21, Subindo, subtitle indonesia, indoxx1, indoxxi';
		}
		$data['top_5'] = $this->CI->Mod_frontend->get_top_5();
		$data['meta_description'] = 'Download Drama korea lengkap subtitle Indonesia';
		$this->CI->load->view($layout, $data);
	}
}