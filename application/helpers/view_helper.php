<?php defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('timestamp_to_date')) {
	function timestamp_to_date($timestamp, $format = 'j M Y - g:i A', $timezone = 'Asia/Jakarta') {
		$date = new DateTime();
		$date->setTimezone(new DateTimeZone($timezone));
		$date->setTimestamp($timestamp);

		return $date->format($format);
	}
}

if (!function_exists('time_to_timestamp')) {
	function date_to_timestamp() {
		$date = new DateTime('NOW');
		$date->setTimezone(new DateTimeZone('UTC'));

		return $date->getTimestamp();
	}
}

if (!function_exists('string_to_timestamp')) {
	function string_to_timestamp($str, $str_format = 'j M Y') {
		$date = DateTime::createFromFormat($str_format, $str);
		if (!$date) {
			return false;
		}

		return $date->getTimestamp();
	}
}

if (!function_exists('money_fmt')) {
	function money_fmt($num) {
		return number_format($num, 0, ',', '.');
	}
}

if (!function_exists('set_get_value')) {
	function set_get_value($field_name, $type = 'input', $field_value = false) {
		if (!isset($_GET[$field_name])) {
			return '';
		}

		$value = $_GET[$field_name];
		if (strtolower($type) == 'input') {
			return 'value="' . $value . '"';
		} elseif (strtolower($type) == 'select' && $field_value !== false) {
			if ($value == $field_value) {
				return 'selected="selected"';
			}
		}

		return '';
	}
}

if (!function_exists('encode_email')) {
	function encode_email($e) {
		for ($i = 0; $i < strlen($e); $i++) {
			$output .= '&#' . ord($e[$i]) . ';';
		}

		return $output;
	}
}

if (!function_exists('base_domain_name')) {
	function base_domain_name($url) {
		$url = trim($url, '/');
		if (!preg_match('#^http(s)?://#', $url)) {
			$url = 'http://' . $url;
		}
		$host         = parse_url($url, PHP_URL_HOST);
		$explode_host = explode('.', $host);
		if (count($explode_host) < 2) {
			return FALSE;
		}

		end($explode_host);
		$last_key = key($explode_host);

		return $explode_host[($last_key - 1)] . '.' . $explode_host[$last_key];
	}
}
