<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="keywords" content="<?php echo $meta_keyword; ?>">
  <link href="<?php echo $url_image_seo; ?>" rel='web-images' type='image/jpeg'>
  <meta name="description" content="<?php echo $meta_description; ?>">
  <meta name="robots" content="index, follow">
  <meta name="googlebot" content="noodp" />
  <meta name="msvalidate.01" content="59A57992E6AF0CBE5EDC8BCCE3634B5F" />
  <meta name="author" content="<?php echo base_url() ?>" />
  <meta content="id" name="language" />
  <meta name="url" content="<?php echo base_url() ?>">
  <meta name="identifier-URL" content="<?php echo base_url() ?>">
  <meta name="directory" content="submission">
  <meta name="category" content="">
  <meta name="coverage" content="Worldwide">
  <meta name="distribution" content="Global">
  <meta name="rating" content="General">
  <meta name="revisit-after" content="7 days">
  <meta http-equiv="Expires" content="0">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Cache-Control" content="no-cache">
  <meta property="og:title" content="<?php echo $title.' - Pecinta Korea Download Drama Korea movie & series'; ?>"/>
  <meta property="og:type" content="movie"/>
  <meta property="og:url" content="<?php print base_url(uri_string());?>"/>
  <meta property="og:image" content="<?php echo $url_image_seo; ?>"/>
  <meta property="og:site_name" content="Pecinta Korea"/>
  <meta property="og:description" content="Pecinta Korea Download film movie & series"/>
  <meta name="og:title" content="<?php echo $title.' - Pecinta Korea Download film movie & series'; ?>"/>
  <meta name="ICBM" content="15.870032, 100.992541" />
  <link rel="canonical" href="<?php print base_url();?>" />
  <meta content="all" name="googlebot" />
  <meta content="all" name="msnbot" />
  <meta content="all" name="Googlebot-Image" />
  <meta content="all" name="Slurp" />
  <meta content="all" name="ZyBorg" />
  <meta content="all" name="Scooter" />
  <meta content="true" name="MSSmartTagsPreventParsing" />
  <meta content="global" name="distribution" />
  <meta content="global" name="target" />
  <meta content="index, follow" name="googlebot" />
  <meta content="follow, all" name="Googlebot-Image" />
  <meta content="follow, all" name="msnbot" />
  <meta content="follow, all" name="Slurp" />
  <meta content="follow, all" name="ZyBorg" />
  <meta content="follow, all" name="Scooter" />
  <meta content="all" name="spiders" />
  <meta content="general" name="rating" />
  <meta name="robots" content="all,index, follow" />
  <meta content="all" name="webcrawlers" />
  <meta content="1 days" name="revisit" />
  <meta name="revisit-after" content="1 days" />
  <meta name="expires" content="never" />
  <meta content="id" name="geo.country" />
  <meta name="netinsert" content="0.0.1.2.23.1.1" />
  <meta content="googlebot, bing, alexa, alltheWeb, altavista, aol netfind, anzwers, canada, directhit, euroseek, excite, overture, go, google, hotbot. infomak, kanoodle, lycos, mastersite, national directory, northern light, searchit, simplesearch, Websmostlinked, webtop, what-u-seek, aol, yahoo, webcrawler, infoseek, excite, magellan, looksmart, aeiwi, cnet, korea, k-drama, dramakorea, drakor, streaming, movie, download, " name="search engines" />
  <meta name="disqus" content="abcdefg"/>
  <meta name="uservoice" content="asdfasdf"/>
  <meta name="mixpanel" content="asdfasdf"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?php echo $title ?> | Pecinta Korea</title>
  <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css') ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/slick.css') ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/slick-theme.css') ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/jquery-ui.css') ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/style.css') ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/auto-complete.css') ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url('assets/css/sosmed.css') ?>" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="icon" href="<?php echo base_url('assets/images/favicon.ico') ?>" type="image/x-icon">
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.bundle.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/slick.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/jquery-ui.js') ?>"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/custom.js') ?>"></script>

</head>
<body>
  <script>
    (function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = '//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.1&appId=2096332517295780&autoLogAppEvents=1';
      fjs.parentNode.insertBefore(js, fjs);
    }
    (document, 'script', 'facebook-jssdk'));
  </script>

<!-- NAV HERE -->
<?php $this->load->view('static/nav') ?>
<!-- /NAV HERE -->

<!--CONTENT HERE-->
<?php $this->load->view($page) ?>
<!--/CONTENT HERE-->

<!--FOOTER-->
<?php $this->load->view('static/footer') ?>

<!--LOAD JS -->
<?php $this->load->view('static/auto_complete') ?>

</body>
</html>