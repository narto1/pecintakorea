<ol class="breadcrumb">
	<?php foreach ($breadcrumb as $key => $value): ?>
		<?php if ($key != 'active_pages'): ?>
			<li class="breadcrumb-item">
				<a href="<?php echo $value ?>"><?php echo $key ?></a>
			</li>
			<?php else: ?>
			<li class="breadcrumb-item active"><?php echo $value ?></li>
		<?php endif ?>
	<?php endforeach ?>
</ol>