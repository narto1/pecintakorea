<script type="text/javascript">
function autoCompleteSearch() {  
  console.log($('#search_mov').outerWidth()+'+'+$('#button_search').outerWidth());
  $( "#search_mov" ).autocomplete({

        source: "<?php echo base_url('home/autocomplete/?');?>"+$("#search_mov").val(),

    focus: function (event, ui) {

        return false;

    },

    select: function (event, ui) {

        jQuery(event.target).val(ui.item.title);

        window.open(ui.item.url, '_blank');

        return false;

    },

    open: function (){

      var offset = $('#search_mov').offset();

      var idSearchInput = $('#search_mov');

      var allListElements = $( "li" );

      var allDivImgElements = $( ".dimg" );

      var allDivDesElements = $( ".ddes" );

      $(allListElements).removeClass('ui-menu-item');

      $(allDivImgElements).removeClass('ui-menu-item-wrapper');

      $(allDivDesElements).removeClass('ui-menu-item-wrapper').attr("style","width:"+($(".menuatas-search-input").outerWidth()-$(".desc").width()-8+"px;"));

      $(".judul").attr("style","max-width:"+($(".menuatas-search-input").outerWidth()-$(".dimg").width()-6)+"px;");

    }

    }).data( "ui-autocomplete" )._renderItem = function( ul, item ) {

    var offset = $('#search_mov').offset();

    var idSearchInput = $('#search_mov'); 

      var divimg=document.createElement("DIV");

      var divdes=document.createElement("DIV");

      var agen=document.createElement("A");

      var ades=document.createElement("A");

      var aimg=document.createElement("A");

      var attl=document.createElement("A");

      var img=document.createElement("IMG");

      var texTit = document.createTextNode(item.title);

      var texDes = document.createTextNode(item.desc);

      var texGen = document.createTextNode(item.genre);

      attl.className="judul";

      divimg.className="dimg";

      divdes.className="ddes";

      $(img).attr({"style":"width:100%;height:100%;"});

      $(aimg).attr({"href":item.url, "class":"desc"});

      $(attl).attr({"href":item.url, "class":"judul"});

      $(agen).attr({"class":"desc"});

      $(ades).attr({"class":"desc"});

      img.setAttribute("src", '<?php echo base_url() ?>'+item.image);

      divimg.appendChild(aimg).appendChild(img);

      divdes.appendChild(attl).appendChild(texTit);

      divdes.appendChild(ades).appendChild(texDes);

      divdes.appendChild(agen).appendChild(texGen);

      $(ul).css({
        "position": "fixed",
        "width"   : ($('#search_mov').outerWidth()+$('#button_search').outerWidth())+"px",
        "left"    : offset.left+"px"
      });

      $(ul).addClass('ul-ac');

      return $( "<li class='list-mov-sug'>" ).css({
        "margin-bottom":"5px",
        "width":($('#search_mov').outerWidth()+$('#button_search').outerWidth())+"px"
      })

      .data( "ui-autocomplete-item", item )  

      .append( divimg ).append( divdes ).appendTo(ul);

     }; 

 }
$(document).ready(function(){
    $("#search_mov").focus(function(){$(this).keydown();});
    $(window).resize(function(){
      $("#search_mov").blur();
    });
});
</script>