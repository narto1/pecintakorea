<div class="container-fluid container-footer">
  <div class="container">
    <div class="row head-footer">
      <div class="col-12 col-sm-12 col-md-6 col-lg-8 col-xl-8 logo-wrap">
        <img src="<?php echo base_url('assets/images/logo-footer.png') ?>">
      </div>
      <div class="col-6 col-sm-6 col-md-3 col-lg-2 col-xl-2 facebook-wrap">
        <img src="<?php echo base_url('assets/images/004-facebook-logo.png') ?>">
        <p>
          <a href="">Facebook</a>
        </p>
      </div>
      <div class="col-6 col-sm-6 col-md-3 col-lg-2 col-xl-2 twitter-wrap">
        <img src="<?php echo base_url('assets/images/003-twitter-black-shape.png') ?>">
        <p>
          <a href="">Twitter</a>
        </p>
      </div>
    </div>
    <div class="row footer-wrap">
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 description-wrap">
        <p>
          Pecintakorea.com merupakan sebuah situs download drama korea terbaik bersubtitle Indonesia yang terupdate dan
          terlengkap, dimana para pecinta film-film drama korea akan dengan mudah mengunduh beragam film drama korea
          secara gratis.
        </p>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 box-wrap">
        <div class="row box-wrapper">
          <?php
            $AZ = ['A'=>'1', 'B'=>'1', 'C'=>'1', 'D'=>'1', 'E'=>'1', 'F'=>'1', 'G'=>'1', 'H'=>'1', 'I'=>'1', 'J'=>'1', 'K'=>'1', 'L'=>'1', 'M'=>'1', 'N'=>'1', 'O'=>'1', 'P'=>'1', 'Q'=>'1', 'R'=>'1', 'S'=>'1', 'T'=>'1', 'U'=>'1', 'V'=>'1', 'W'=>'1', 'X'=>'1', 'Y'=>'1', 'Z'=>'1'];
          ?>
          <?php foreach ($AZ as $key => $value): ?>
            <div class="columns">
              <div class="card">
                <div class="card-body">
                  <a href="<?php echo base_url('list/'.$key) ?>">
                    <h5 class="card-title"><?php echo $key ?></h5>
                  </a>
                </div>
              </div>
            </div>
          <?php endforeach ?>
          
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 list-wrap">
        <div class="row list-wrapper">
          <div class="col-6 list-top-movie">
            <h4>TOP 5 List</h4>
            <?php foreach ($top_5 as $key): ?>
              <p><?php echo $key->genre.' , '.explode('-', $key->release_date)[0] ?></p>
              <h5>
                <?php if ($key->jenis === 'movie'): ?>
                  <a target="blank" href="<?php echo base_url('movie/'.$key->slug) ?>"><?php echo $key->title ?></a>
                  <?php else: ?>
                  <a target="blank" href="<?php echo base_url('series/'.$key->slug) ?>"><?php echo $key->title ?></a>
                <?php endif ?>
              </h5>
            <?php endforeach ?>
          </div>
          <div class="col-6 list-about">
            <ul>
              <li>
                <a href="<?php echo base_url('faq') ?>">FAQ</a>
              </li>
              <li>
                <a href="<?php echo base_url('dmca') ?>">DMCA</a>
              </li>
              <li>
                <a href="<?php echo base_url('request') ?>">REQUEST</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid copyright">
  <p>&copy; 2018 pecintakorea.com All right reserved</p>
</div>