<div class="button-wrap text-center d-block">
  <div class="share-social-media">
    <?php $new_url_share = str_replace("/", "%2F", "http://pecintakorea.com/".uri_string()); ?>
    <div class="share-btn">
      <a id="fb_share" class="fa fa-facebook"></a>
      <a id="line_share" class="fa fa-line"></a>
      <a id="g_share" class="fa fa-google"></a>
      <a id="wa_share" href="<?php echo 'whatsapp://send?abid=phonenumber&text='.base_url(uri_string()); ?>" class="fa fa-whatsapp"></a>
    </div>
  </div>
</div>