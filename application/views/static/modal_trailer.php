<style type="text/css">
.modal {
  background-color: #15141485;
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px;
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}

/* footer footnotes */
footer ol {
  border-top: 1px solid #eee;
  margin-top: 40px;
  padding-top: 15px;
  padding-left: 20px;
}

/* Bootstrap Docs */
.bs-example {
    position: relative;
    padding: 45px 15px 15px;
    margin: 0 -15px 15px;
    border-color: #e5e5e5 #eee #eee;
    border-style: solid;
    border-width: 1px 0;
    -webkit-box-shadow: inset 0 3px 6px rgba(0,0,0,.05);
    box-shadow: inset 0 3px 6px rgba(0,0,0,.05);
}
.bs-example:after {
    position: absolute;
    top: 15px;
    left: 15px;
    font-size: 12px;
    font-weight: 700;
    color: #959595;
    text-transform: uppercase;
    letter-spacing: 1px;
    content: "Example";
}
.bs-example-padded-bottom {
    padding-bottom: 24px;
}
@media (min-width: 768px){
  .bs-example {
      margin-right: 0;
      margin-left: 0;
      background-color: #fff;
      border-color: #ddd;
      border-width: 1px;
      border-radius: 4px 4px 0 0;
      -webkit-box-shadow: none;
      box-shadow: none;
  }
}
.bs-example+.code {
    margin: -15px -15px 15px;
    border-width: 0 0 1px;
    border-radius: 0;
}
@media (min-width: 768px){
  .bs-example+.code {
      margin-top: -16px;
      margin-right: 0;
      margin-left: 0;
      border-width: 1px;
      border-bottom-right-radius: 4px;
      border-bottom-left-radius: 4px;
  }
}
/* CodeMirror Bootstrap Theme */
.cm-s-bootstrap .cm-comment {
  font-style: italic;
  color: #999988;
}
.cm-s-bootstrap .cm-number {
  color: #F60;
}
.cm-s-bootstrap .cm-atom {
  color: #366;
}
.cm-s-bootstrap .cm-variable-2 {
  color: #99F;
}
.cm-s-bootstrap .cm-property {
  color: #99F;
}
.cm-s-bootstrap .cm-string {
  color: #DD1144;
}
.cm-s-bootstrap .cm-keyword {
  color: #069;
}
.cm-s-bootstrap .cm-operator {
  color: #555;
}
.cm-s-bootstrap .cm-qualifier {
  color: #0A8;
}
</style>
<!-- Modal -->
  <div class="modal" id="myLargeModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" onclick="hide_trailer()">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close p-0 mr-1" onclick="hide_trailer()">&times;</button>
        </div>
        <div class="modal-body">
          <iframe id="frame_trailer" width="100%" height="100%" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
<script type="text/javascript">
function setModalMaxHeight(element) {
  this.$element     = $(element);  
  this.$content     = this.$element.find('.modal-content');
  var borderWidth   = this.$content.outerHeight() - this.$content.innerHeight();
  var dialogMargin  = $(window).width() < 768 ? 20 : 60;
  var contentHeight = $(window).height() - (dialogMargin + borderWidth);
  var headerHeight  = this.$element.find('.modal-header').outerHeight() || 0;
  var footerHeight  = this.$element.find('.modal-footer').outerHeight() || 0;
  var maxHeight     = contentHeight - (headerHeight + footerHeight);

  this.$content.css({
      'overflow': 'hidden'
  });
  
  this.$element
    .find('.modal-body').css({
      'max-height': maxHeight,
      'overflow-y': 'auto'
    });
}

$('.modal').on('show.bs.modal', function() {
  $(this).show(); 
  setModalMaxHeight(this);
});

$(window).resize(function() {
  if ($('.modal.in').length == 1) {
    setModalMaxHeight($('.modal.in'));
  }
});

/* CodeMirror */
$('.code').each(function() {
  var $this = $(this),
      $code = $this.text(),
      $mode = $this.data('language');

  $this.empty();
  $this.addClass('cm-s-bootstrap');
  CodeMirror.runMode($code, $mode, this);
});

function show_trailer(src)
{
  $('#frame_trailer').attr('src', src+'?rel=0&enablejsapi=1&version=3&playerapiid=ytplayer');
  $('#myLargeModal').css('display', 'block');
}

function hide_trailer()
{
  $('#frame_trailer')[0].contentWindow.postMessage('{"event":"command","func":"' + 'stopVideo' + '","args":""}', '*');
  $('#myLargeModal').css('display', 'none');
}
</script>