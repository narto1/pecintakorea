<nav id="navbar" class="navbar fixed-top navbar-expand-lg navbar-light">
  <a class="navbar-brand" href="<?php echo base_url() ?>"><img src="<?php echo base_url('assets/images/lOGO1.png') ?>"></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto ml-auto">
      <li class="nav-item">
        <span><img src="<?php echo base_url('assets/images/Layer-26.png') ?>"></span><a class="nav-link" href="<?php echo base_url('terpopuler/1') ?>">TERPOPULER</a>
      </li>
      <li class="nav-item">
        <span><img src="<?php echo base_url('assets/images/Layer-25.png') ?>"></span><a class="nav-link" href="<?php echo base_url('terbaru/1') ?>">TERBARU</a>
      </li>
      <li class="nav-item">
        <span><img src="<?php echo base_url('assets/images/Layer-23.png') ?>"></span><a class="nav-link" href="<?php echo base_url('ongoing/1') ?>">ON GOING</a>
      </li>
      <li class="nav-item mydropdown">
        <span><img src="<?php echo base_url('assets/images/Layer-24.png') ?>"></span>
        <a class="nav-link mydropbtn" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
          GENRES
          <i class="fa fa-caret-down"></i>
        </a>
        <div class="mydropdown-content" aria-labelledby="navbarDropdown">
          <a class="dropdown-a" href=<?php echo base_url("genre/Comedy/1") ?>>Comedy</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Action/1") ?>>Action</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Adventure/1") ?>>Adventure</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Romance/1") ?>>Romance</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Medical/1") ?>>Medical</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Melodrama/1") ?>>Melodrama</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Vampire/1") ?>>Vampire</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Thriller/1") ?>>Thriller</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Horror/1") ?>>Horror</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Family/1") ?>>Family</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Drama/1") ?>>Drama</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Period/1") ?>>Period</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Fantasy/1") ?>>Fantasy</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Mystery/1") ?>>Mystery</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Crime/1") ?>>Crime</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Mockumentary/1") ?>>Mockumentary</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Workplace/1") ?>>Workplace</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/School/1") ?>>School</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Science/1") ?>>Science</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Supernatural/1") ?>>Supernatural</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Tragedy/1") ?>>Tragedy</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Suspense/1") ?>>Suspense</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Teen/1") ?>>Teen</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Youth/1") ?>>Youth</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Food/1") ?>>Food</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Sci-fi/1") ?>>Sci-fi</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Variety-Show/1") ?>>Variety Show</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Revenge/1") ?>>Revenge</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Psychological/1") ?>>Psychological</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Woman/1") ?>>Woman</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Social-Issues/1") ?>>Social Issues</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Time-Travel/1") ?>>Time Travel</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Reality-Show/1") ?>>Reality Show</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Legal/1") ?>>Legal</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Musical/1") ?>>Musical</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Sport/1") ?>>Sport</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Web-Drama/1") ?>>Web-drama</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Work/1") ?>>Work</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Epic/1") ?>>Epic</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Zombie/1") ?>>Zombie</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Documentary/1") ?>>Documentary</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Human/1") ?>>Human</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Office/1") ?>>Office</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Music/1") ?>>Music</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Political/1") ?>>Political</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Friendship/1") ?>>Friendship</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/Travel/1") ?>>Travel</a>
          <a class="dropdown-a" href=<?php echo base_url("genre/TV-Movie/1") ?>>TV Movie</a>
        </div>
      </li>
    </ul>
    <form action="<?php echo base_url('search_keyword') ?>" method="post" class="form-inline my-2 my-lg-0">
      <input onkeypress="autoCompleteSearch()" id="search_mov" class="form-control search-form" type="search" aria-label="Search" name="post_judul">
      <button class="btn btn-outline-success search-button my-2 my-sm-0" type="submit" id="button_search">Search</button>
    </form>
  </div>
</nav>