<div class="mt-5r mw-14"></div>
<?php $this->load->view('static/modal_trailer') ?>


<?php $this->load->view('static/horizontal_banner') ?>

<div class="container-fluid container-movie-popular">
  <div class="row movie-popular-wrapper">
    <div class="col-12 col-md-9 col-lg-9 col-xl-9 movie-popular-wrap">
      <div class="row sorting-wrapper">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 title">
          <h2><?php echo $title ?></h2>
        </div>
      </div>
      <div class="row movie-wrapper">
        <?php foreach ($post_data as $key): ?>
          <div class="element-wrap col-lg-2 col-md-3 col-sm-4 col-4">
            <?php
              if ($key->jenis === 'movie') {
                if ($key->quality === 'bluray') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'BLU';
                }
                if ($key->quality === 'cam') {
                  $quality = 'complete';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'CAM';
                }
                if ($key->quality === 'hd' || $key->quality === 'fhd') {
                  $quality = 'HD';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'HD';
                }
                if ($key->quality === 'sd') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'SD';
                }
              }else{
                if ($key->progres === 'ongoing') {
                  $quality = 'ongoing';
                  $quality_col_right = 'col-6';
                  $quality_col_left = 'col-6';
                  $quality_text = 'Eps.'.$key->episode;
                }else{
                  $quality = 'complete';
                  $quality_col_right = 'col-7';
                  $quality_col_left = 'col-5';
                  $quality_text = 'Complete';
                }
              }
            ?>
            <div class="card <?php echo $quality ?>">
              <div class="hover">
                <img class="card-img-top" src="<?php echo base_url($key->url_image) ?>" alt="Card image cap">
                <div class="overlay">
                  <div class="cursor-pointer text-trailer">
                    <a onclick="show_trailer('<?php echo $key->url_trailer ?>')">Trailer</a>
                  </div>
                  <div class="text-movie">
                    <?php if ($key->jenis === 'movie'): ?>
                      <a href="<?php echo base_url('movie/'.$key->slug) ?>">Download</a>
                      <?php else: ?>
                      <a href="<?php echo base_url('series/'.$key->slug) ?>">Download</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
              <div class="text-block">
                <div class="row text-block-wrapper">
                  <div class="<?php echo $quality_col_left ?> left">
                    <img src="<?php echo base_url('assets/images/Layer-6-copy-13.png') ?>">
                    <h4><?php echo $this->Mod_home->get_movie_imdb($key->movie_imdb_id)->imdbRating; ?></h4>
                  </div>
                  <div class="<?php echo $quality_col_right ?> right-wrap">
                    <div class="right">
                      <p><?php echo $quality_text ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <p><?php echo $key->genre ?>, <?php echo explode('-', $key->release_date)[0] ?></p>
                <h5 class="card-title"><?php echo $key->title ?></h5>
              </div>
            </div>
          </div>
        <?php endforeach ?>
          
      </div>
      <?php $totalpage = $total_row/24 ?>

      <div style="width: 100%;flex: 0 0 100%;position: relative; text-align:center">
        <?php if ($page_ > 1): ?>
          <?php  
            $Sebelumnya = $page_ - 1;
            $Berikutnya = $page_ + 1;
          ?>
          <div style="margin:5px auto auto auto;">
            <a href='<?php echo base_url($page_routes."/".$Sebelumnya) ?>' class="btn btn-outline-primary">Sebelumnya</a>
            <?php if ($page_ > 2): ?>
              <?php $page_Prev = $page_-2; ?>
              <a href='<?php echo base_url($page_routes."/".$page_Prev) ?>' class="btn btn-outline-secondary" style="margin-left:4px;"><?php echo $page_Prev ?></a>
              <a href='<?php echo base_url($page_routes."/".$Sebelumnya) ?>' class="btn btn-outline-secondary" style="margin-left:4px;"><?php echo $Sebelumnya ?></a>
            <?php endif ?>
            <a class="btn btn-secondary active" role="button" aria-pressed="true" style="margin-left:4px;"><?php echo $page_ ?></a>
            <?php if (abs($totalpage) > $page_): ?>
              <?php $page_Next = $page_+1; ?>
              <a href='<?php echo base_url($page_routes."/".$page_Next) ?>' class="btn btn-outline-secondary"><?php echo $page_Next ?></a>
              <?php if (abs($totalpage) > $page_Next): ?>
                <?php $page_Next = $page_+2; ?>
                <a href='<?php echo base_url($page_routes."/".$page_Next) ?>' class="btn btn-outline-secondary"><?php echo $page_Next ?></a>
              <?php endif ?>
            <?php endif ?>
            <?php if ($page_ < $totalpage): ?>
              <a href='<?php echo base_url($page_routes."/".$Berikutnya) ?>' class="btn btn-outline-primary">Berikutnya</a>
            <?php endif ?>
          </div>
      <?php elseif ($totalpage > 1): ?>
        <a href='<?php echo base_url($page_routes."/2") ?>' class="btn btn-outline-primary">Load More</a>
      <?php endif ?>
    </div>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
      <?php $this->load->view('static/vertical_banner') ?>
    </div>

  </div>
</div>