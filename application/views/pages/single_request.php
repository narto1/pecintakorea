<?php $this->load->view('static/modal_trailer') ?>
<div class="ontainer-fluid  container-about-us mt-5r mw-14">
  <div class="col-12">
    <?php $this->load->view('static/horizontal_banner') ?>
  </div>
  <div class="row about-wrapper">
    <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8 detail-about-wrap pl-4 pr-4">
      <h1>Mau Request Film? submit disini!</h1>
      <form accept="<?php echo base_url('request') ?>" method="post">
        <?php if ($this->session->flashdata('success')): ?>
          <div class="form-group">
            <div class="alert alert-success">
              <?php echo $this->session->flashdata('success') ?>
            </div>
          </div>
        <?php endif ?>
        <div class="form-group row">
          <label for="inputEmail" class="col-sm-2 col-form-label">E-mail :</label>
          <div class="col-sm-10">
            <input type="email" class="form-control" id="inputEmail" name="post_email">
            <?php echo form_error('post_email', '<li class="text-danger">', '</li>'); ?>
          </div>
        </div>
        <div class="form-group row">
          <label for="inputTitle" class="col-sm-2 col-form-label">Judul Film :</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" id="inputTitle" name="post_judul">
            <?php echo form_error('post_judul', '<li class="text-danger">', '</li>'); ?>
          </div>
        </div>
        <div class="form-group row">
          <div class="col-sm-10"></div>
          <div class="col-sm-2">
            <button type="submit" class="btn btn-primary">REQUEST!</button>
          </div>
        </div>
      </form>
    </div>
    <div class="col-md-5 col-lg-4 col-xl-4 mt-3 mb-3">
      <?php $this->load->view('static/vertical_banner') ?>
    </div>
  </div>
</div>