<?php $this->load->view('static/modal_trailer') ?>
<div class="container-fluid container-content mt-5r mw-14">
  <div class="row content-wrapper">
    <div class="col-12">
      <?php $this->load->view('static/horizontal_banner') ?>
    </div>
    <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8 detail-wrap pl-4 pr-4 mt-3">
      <div class="row mb-3">
        <div class="col-md-3">
          <img style="width: 100%" src="<?php echo base_url($movie_data->url_image) ?>">
        </div>
        <div class="col-md-9">
          <div class="title-movie-wrap">
            <h1 class="title"><?php echo $movie_data->title . ' (' . explode('-', $movie_data->release_date)[0] . ')' ?></h1>
          </div>
          <p class="synopsis-movie">
            <?php echo $movie_data->overview; ?>
          </p>
        </div>
      </div>
      <div class="line-eps">
        <ul class="row">
          <?php foreach ($all_episode as $key): ?>
            <?php if ($key->episode === $episode_data->episode): ?>
              <li class="active"><?php echo 'Eps. '.$key->episode ?></li>
              <?php else: ?>
              <a href="<?php echo base_url('series/'.$movie_data->slug.'/'.$key->episode) ?>">
                <li><?php echo 'Eps. '.$key->episode ?></li>
              </a>
            <?php endif ?>
          <?php endforeach ?>
        </ul>
      </div>
      <div class="box-detail">
        <ul>
          <li>Type : <?php echo ucfirst($movie_data->jenis) ?></li>
          <li>IMDB Rating : 7.4</li>
          <li>
            Genre :
            <?php $array_genre = explode(',', $movie_data->genre) ?> 
            <?php for ($i=0; $i < count($array_genre); $i++): ?>
              <a href="<?php echo base_url('genre/'.$array_genre[$i]) ?>"><?php echo $array_genre[$i] ?></a>
              <?php
                if ($i+1 < count($array_genre)) {
                  echo ", ";
                }
              ?>
            <?php endfor ?>
          </li>
          <li>
            Actor : 
            <?php $array_actor = explode(',', $movie_data->actor);?> 
            <?php for ($i=0; $i < count($array_actor); $i++): ?>
              <a href="<?php echo base_url('actor/'.$array_actor[$i]) ?>"><?php echo $array_actor[$i] ?></a>
              <?php if ($i+1 < count($array_actor)): ?>
                <?php echo ", " ?>
              <?php endif ?>
            <?php endfor ?>
          </li>
          <li>Director : <a href="<?php echo base_url('director/'.$movie_data->director) ?>"><?php echo $movie_data->director ?></a></li>
          <li>
            Release Date : 
            <?php 
              $date = new DateTime($movie_data->release_date);
              echo $date->format('j M Y');
            ?>
          </li>
          <li>Production : <a href="<?php echo base_url('production/'.$movie_data->produksi) ?>"><?php echo $movie_data->produksi ?></a></li>
          <li>Country : <a href="<?php echo base_url('country/'.$movie_data->country) ?>"><?php echo $movie_data->country ?></a></li>
        </ul>
      </div>
      <?php $this->load->view('static/share') ?>
      <div class="button-wrap text-center d-block">
            <div class="p-2">
              <button class="btn btn-lg">
                Download Now
              </button>
            </div>
          <div>
            <div class="p-2 d-inline">
              <a target="blank" href="<?php echo 'https://indodownload.com/hidup-indonesiah-merdekah-sejaterah-sentosah/?aisyah-jatuh-cintah-samah-jamilah='.md5($episode_data->url_download_1); ?>" class="btn btn-sm">
                <span><img src="<?php echo base_url('assets/images/stream/download-archive-tray.png') ?>"></span>
                Server 1
              </a>
            </div>
            <div class="p-2 d-inline">
              <a target="blank" href="<?php echo 'https://indodownload.com/hidup-indonesiah-merdekah-sejaterah-sentosah/?aisyah-jatuh-cintah-samah-jamilah='.md5($episode_data->url_download_2); ?>" class="btn btn-sm">
                <span><img src="<?php echo base_url('assets/images/stream/download-archive-tray.png') ?>"></span>
                Server 2
              </a>
            </div>
            <div class="p-2 d-inline">
              <a target="blank" href="<?php echo 'https://indodownload.com/hidup-indonesiah-merdekah-sejaterah-sentosah/?aisyah-jatuh-cintah-samah-jamilah='.md5($episode_data->url_download_3); ?>" class="btn btn-sm">
                <span><img src="<?php echo base_url('assets/images/stream/download-archive-tray.png') ?>"></span>
                Server 3
              </a>
            </div>
            <?php if ($movie_data->url_sub != '#'): ?>
              <div class="button-wrap">
                <div class="p-2">
                  <a href="<?php echo base_url($movie_data->url_sub) ?>" class="btn btn-sm">
                    <span><img src="<?php echo base_url('assets/images/stream/download-archive-tray.png') ?>"></span>
                    Subtitle Indo
                  </a>
                </div>
              </div>
            <?php endif ?>
          </div>
      </div>
      <div class="tag-line-movie">
        <ul class="row">
          <?php  
            $tag = str_replace('-', ' ', $movie_data->tag);
            $split_tag = explode(',', $tag);
          ?>
          <?php for ($i=0; $i < count($split_tag); $i++): ?>
            <li><?php echo $split_tag[$i] ?></li>
          <?php endfor ?>
        </ul>
      </div>
      <div class="related-movie">
        <div class="title-wrap">
          <h1 class="title">Related Movie</h1>
        </div>
        <div class="row movie-wrapper">
          <?php foreach ($related_post as $key): ?>
          <div class="element-wrap col-6 col-sm-4 col-md-4 col-lg-3">
            <?php
              if ($key->jenis === 'movie') {
                if ($key->quality === 'bluray') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'BLU';
                }
                if ($key->quality === 'cam') {
                  $quality = 'complete';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'CAM';
                }
                if ($key->quality === 'hd' || $key->quality === 'fhd') {
                  $quality = 'HD';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'HD';
                }
                if ($key->quality === 'sd') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'SD';
                }
              }else{
                if ($key->progres === 'ongoing') {
                  $quality = 'ongoing';
                  $quality_col_right = 'col-6';
                  $quality_col_left = 'col-6';
                  $quality_text = 'Eps.'.$key->episode;
                }else{
                  $quality = 'complete';
                  $quality_col_right = 'col-7';
                  $quality_col_left = 'col-5';
                  $quality_text = 'Complete';
                }
              }
            ?>
            <div class="card <?php echo $quality ?>">
              <div class="hover">
                <img class="card-img-top" src="<?php echo base_url($key->url_image) ?>" alt="Card image cap">
                <div class="overlay">
                  <div class="cursor-pointer text-trailer">
                    <a onclick="show_trailer('<?php echo $key->url_trailer ?>')">Trailer</a>
                  </div>
                  <div class="text-movie">
                    <?php if ($key->jenis === 'movie'): ?>
                      <a href="<?php echo base_url('movie/'.$key->slug) ?>">Download</a>
                      <?php else: ?>
                      <a href="<?php echo base_url('series/'.$key->slug) ?>">Download</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
              <div class="text-block">
                <div class="row text-block-wrapper">
                  <div class="<?php echo $quality_col_left ?> left">
                    <img src="<?php echo base_url('assets/images/Layer-6-copy-13.png') ?>">
                    <h4><?php echo $this->Mod_home->get_movie_imdb($key->movie_imdb_id)->imdbRating; ?></h4>
                  </div>
                  <div class="<?php echo $quality_col_right ?> right-wrap">
                    <div class="right">
                      <p><?php echo $quality_text ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <p><?php echo $key->genre ?>, <?php echo explode('-', $key->release_date)[0] ?></p>
                <h5 class="card-title"><?php echo $key->title ?></h5>
              </div>
            </div>
          </div>
          <?php endforeach ?>
        </div>
      </div>
      <?php $this->load->view('static/comment') ?>
    </div>
    <div class="col-md-5 col-lg-4 col-xl-4 mt-3 mb-3">
      <?php $this->load->view('static/vertical_banner') ?>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $(".responsive").slick({
      dots          : true,
      prevArrow     : $('.prev'),
      nextArrow     : $('.next'),
      infinite      : true,
      speed         : 300,
      slidesToShow  : 6,
      slidesToScroll: 6,
      responsive    : [
        {
          breakpoint: 1024,
          settings  : {
            slidesToShow  : 5,
            slidesToScroll: 5,
            infinite      : true,
            dots          : true
          }
        },
        {
          breakpoint: 600,
          settings  : {
            slidesToShow  : 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 480,
          settings  : {
            slidesToShow  : 3,
            slidesToScroll: 3
          }
        }
      ]
    });
  });
</script>