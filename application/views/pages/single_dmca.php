<?php $this->load->view('static/modal_trailer') ?>
<div class="ontainer-fluid  container-about-us mt-5r mw-14">
  <div class="col-12">
    <?php $this->load->view('static/horizontal_banner') ?>
  </div>
  <div class="row about-wrapper">
    <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8 detail-about-wrap pl-4 pr-4">
      <h1>DMCA</h1>
      <p>
        Adalah sebuah website penyedia jasa hiburan download film online yang berisikan konten-konten film Korea dan
        Asia Timur dari hasil pencarian yang ada di internet (seperti Google, Yahoo, Yandex, Bing, dan lainnya). Kami
        tidak menyimpan atau melakukan cache terhadap file film tersebut dalam server kami, kami hanya menautkan. Kami
        sangat menyarankan Anda untuk mendukung pemilik karya cipta dengan cara membeli media secara legal agar
        pemilik hak cipta dapat terus berkarya.<br/><br/>


        Kami sangat mendukung akan hukum hak cipta dan akan sangat melindungi hukum pemilik hak cipta secara serius.
        <br/><br/>

        Jika anda adalah pemilik konten apapun yang muncul pada website Pecintakorea.com dan Anda tidak mengizinkan
        penggunaan konten tersebut, maka Anda sangat diperkenankan untuk memberitahu kami dengan menulis email kepada
        dmca@pk.com agar kami dapat mengidentifikasi dan mengambil tindakan yang diperlukan.
        <br/><br/>

        Kami tidak dapat mengambil tindakan jika anda tidak memberikan kami informasi yang diperlukan. Jadi silahkan
        menuliskan email yang diisi dengan berisikan beberapa detail yang disebutkan dibawah ini.
        <br/><br/>

        Spesifikasikan konten hak cipta yang menurut anda telah dilanggar. Jika anda mengklaim pelanggaran dari
        beberapa karya cipta dalam satu email maka silahkan menuliskan daftar tersebut secara detail beserta alamat
        website yang berisikan konten yang melanggar tersebut. Informasikan nama anda, telepon, alamat kantor dan
        alamat email untuk memungkinkan kami menghubungi anda.
        <br/><br/>

        Jika anda memiliki itikad yang baik maka kirimkan email tersebut sendiri, tidak diizinkan apabila email
        tersebut dari agen atau pihak ketiga.
        <br/><br/>

        Informasi yang dituliskan harus akurat dan berada dibawah hukuman pemalsuan.


      </p>
    </div>
    <div class="col-md-5 col-lg-4 col-xl-4 mt-3 mb-3">
      <?php $this->load->view('static/vertical_banner') ?>
    </div>
  </div>
</div>