<?php $this->load->view('static/modal_trailer') ?>
<div class="container-fluid container-movie-featured mt-5">
  <div class="row featured-wrapper">
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 featured-wrap">
      <div class="row">
        <div class="col-12 featured text-center">
          <hr/>
          <h1>Featured</h1>
        </div>
      </div>
    </div>
    <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 movie-wrap">
      <div class="row movie-wrapper responsive">
        <?php foreach ($featured_post as $key): ?>
        <div class="element-wrap">
          <?php
            if ($key->jenis === 'movie') {
              if ($key->quality === 'bluray') {
                $quality = 'bluray';
                $quality_col_right = 'col-5';
                $quality_col_left = 'col-7';
                $quality_text = 'BLU';
              }
              if ($key->quality === 'cam') {
                $quality = 'complete';
                $quality_col_right = 'col-5';
                $quality_col_left = 'col-7';
                $quality_text = 'CAM';
              }
              if ($key->quality === 'hd' || $key->quality === 'fhd') {
                $quality = 'HD';
                $quality_col_right = 'col-5';
                $quality_col_left = 'col-7';
                $quality_text = 'HD';
              }
              if ($key->quality === 'sd') {
                $quality = 'bluray';
                $quality_col_right = 'col-5';
                $quality_col_left = 'col-7';
                $quality_text = 'SD';
              }
            }else{
              if ($key->progres === 'ongoing') {
                $quality = 'ongoing';
                $quality_col_right = 'col-6';
                $quality_col_left = 'col-6';
                $quality_text = 'Eps.'.$key->episode;
              }else{
                $quality = 'complete';
                $quality_col_right = 'col-7';
                $quality_col_left = 'col-5';
                $quality_text = 'Complete';
              }
            }
          ?>
          <div class="card <?php echo $quality ?>">
            <div class="hover">
              <img class="card-img-top" src="<?php echo base_url($key->url_image) ?>" alt="Card image cap">
              <div class="overlay">
                <div class="cursor-pointer text-trailer">
                  <a onclick="show_trailer('<?php echo $key->url_trailer ?>')">Trailer</a>
                </div>
                <div class="text-movie">
                  <?php if ($key->jenis === 'movie'): ?>
                    <a href="<?php echo base_url('movie/'.$key->slug) ?>">Download</a>
                    <?php else: ?>
                    <a href="<?php echo base_url('series/'.$key->slug) ?>">Download</a>
                  <?php endif ?>
                </div>
              </div>
            </div>
            <div class="text-block">
              <div class="row text-block-wrapper">
                <div class="<?php echo $quality_col_left ?> left">
                  <img src="<?php echo base_url('assets/images/Layer-6-copy-13.png') ?>">
                  <h4><?php echo $this->Mod_home->get_movie_imdb($key->movie_imdb_id)->imdbRating; ?></h4>
                </div>
                <div class="<?php echo $quality_col_right ?> right-wrap">
                  <div class="right">
                    <p><?php echo $quality_text ?></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-body">
              <p><?php echo $key->genre ?>, <?php echo explode('-', $key->release_date)[0] ?></p>
              <h5 class="card-title"><?php echo $key->title ?></h5>
            </div>
          </div>
        </div>
      <?php endforeach ?>
      </div>
    </div>
  </div>
</div>

<?php $this->load->view('static/horizontal_banner') ?>

<div class="container-fluid container-movie-popular">
  <div class="row movie-popular-wrapper">
    <div class="col-12 col-md-9 col-lg-9 col-xl-9 movie-popular-wrap">
      <div class="row sorting-wrapper">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 title">
          <h2>Terpopuler</h2>
        </div>
      </div>
      <div class="row movie-wrapper">
        <?php foreach ($popular_post as $key): ?>
          <div class="element-wrap col-lg-2 col-md-3 col-sm-4 col-4">
            <?php
              if ($key->jenis === 'movie') {
                if ($key->quality === 'bluray') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'BLU';
                }
                if ($key->quality === 'cam') {
                  $quality = 'complete';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'CAM';
                }
                if ($key->quality === 'hd' || $key->quality === 'fhd') {
                  $quality = 'HD';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'HD';
                }
                if ($key->quality === 'sd') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'SD';
                }
              }else{
                if ($key->progres === 'ongoing') {
                  $quality = 'ongoing';
                  $quality_col_right = 'col-6';
                  $quality_col_left = 'col-6';
                  $quality_text = 'Eps.'.$key->episode;
                }else{
                  $quality = 'complete';
                  $quality_col_right = 'col-7';
                  $quality_col_left = 'col-5';
                  $quality_text = 'Complete';
                }
              }
            ?>
            <div class="card <?php echo $quality ?>">
              <div class="hover">
                <img class="card-img-top" src="<?php echo base_url($key->url_image) ?>" alt="Card image cap">
                <div class="overlay">
                  <div class="cursor-pointer text-trailer">
                    <a onclick="show_trailer('<?php echo $key->url_trailer ?>')">Trailer</a>
                  </div>
                  <div class="text-movie">
                    <?php if ($key->jenis === 'movie'): ?>
                      <a href="<?php echo base_url('movie/'.$key->slug) ?>">Download</a>
                      <?php else: ?>
                      <a href="<?php echo base_url('series/'.$key->slug) ?>">Download</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
              <div class="text-block">
                <div class="row text-block-wrapper">
                  <div class="<?php echo $quality_col_left ?> left">
                    <img src="<?php echo base_url('assets/images/Layer-6-copy-13.png') ?>">
                    <h4><?php echo $this->Mod_home->get_movie_imdb($key->movie_imdb_id)->imdbRating; ?></h4>
                  </div>
                  <div class="<?php echo $quality_col_right ?> right-wrap">
                    <div class="right">
                      <p><?php echo $quality_text ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <p><?php echo $key->genre ?>, <?php echo explode('-', $key->release_date)[0] ?></p>
                <h5 class="card-title"><?php echo $key->title ?></h5>
              </div>
            </div>
          </div>
        <?php endforeach ?>
          
      </div>
      <h1 class="load-more"><a href="<?php echo base_url('terpopuler') ?>">+ Load More</a></h1>
    </div>
    <div class="col-md-3 col-lg-3 col-xl-3">
      <?php $this->load->view('static/vertical_banner') ?>
    </div>

  </div>
</div>
<div class="container-fluid container-movie-latest">
  <div class="container">
    <div class="row sorting-wrapper">
      <div class="col-12 col-sm-12 col-md-4 col-lg-6 col-xl-6 title">
        <h2>Terbaru</h2>
      </div>
      <div class="col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6 sorting">
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#">Today</a></li>
            <li class="breadcrumb-item"><a href="#">This Week</a></li>
            <li class="breadcrumb-item"><a href="#">This Month</a></li>
            <li class="breadcrumb-item"><a href="#">Last 3 Months</a></li>
          </ol>
        </nav>
        <h4><a style="color: #fff" href="<?php echo base_url('terbaru') ?>">View All</a></h4>
      </div>
    </div>
    <div class="row movie-wrapper">
      <?php foreach ($latest_post as $key): ?>
          <div class="element-wrap col-lg-2 col-md-3 col-sm-4 col-4">
            <?php
              if ($key->jenis === 'movie') {
                if ($key->quality === 'bluray') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'BLU';
                }
                if ($key->quality === 'cam') {
                  $quality = 'complete';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'CAM';
                }
                if ($key->quality === 'hd' || $key->quality === 'fhd') {
                  $quality = 'HD';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'HD';
                }
                if ($key->quality === 'sd') {
                  $quality = 'bluray';
                  $quality_col_right = 'col-5';
                  $quality_col_left = 'col-7';
                  $quality_text = 'SD';
                }
              }else{
                if ($key->progres === 'ongoing') {
                  $quality = 'ongoing';
                  $quality_col_right = 'col-6';
                  $quality_col_left = 'col-6';
                  $quality_text = 'Eps.'.$key->episode;
                }else{
                  $quality = 'complete';
                  $quality_col_right = 'col-7';
                  $quality_col_left = 'col-5';
                  $quality_text = 'Complete';
                }
              }
            ?>
            <div class="card <?php echo $quality ?>">
              <div class="hover">
                <img class="card-img-top" src="<?php echo base_url($key->url_image) ?>" alt="Card image cap">
                <div class="overlay">
                  <div class="cursor-pointer text-trailer">
                    <a onclick="show_trailer('<?php echo $key->url_trailer ?>')">Trailer</a>
                  </div>
                  <div class="text-movie">
                    <?php if ($key->jenis === 'movie'): ?>
                      <a href="<?php echo base_url('movie/'.$key->slug) ?>">Download</a>
                      <?php else: ?>
                      <a href="<?php echo base_url('series/'.$key->slug) ?>">Download</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
              <div class="text-block">
                <div class="row text-block-wrapper">
                  <div class="<?php echo $quality_col_left ?> left">
                    <img src="<?php echo base_url('assets/images/Layer-6-copy-13.png') ?>">
                    <h4><?php echo $this->Mod_home->get_movie_imdb($key->movie_imdb_id)->imdbRating; ?></h4>
                  </div>
                  <div class="<?php echo $quality_col_right ?> right-wrap">
                    <div class="right">
                      <p><?php echo $quality_text ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <p><?php echo $key->genre ?>, <?php echo explode('-', $key->release_date)[0] ?></p>
                <h5 class="card-title"><?php echo $key->title ?></h5>
              </div>
            </div>
          </div>
        <?php endforeach ?>
      </div>
    </div>
  </div>
</div>
<div class="container-fluid container-movie-ongoing">
  <div class="container">
    <div class="row ongoing-wrapper">
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3 ongoing-wrap">
        <div class="row">
          <div class="col-12 ongoing">
            <hr/>
            <h1>On Going</h1>
          </div>
          <div class="col-12 view">
            <h5><a style="color:#24baef" href="<?php echo base_url('ongoing') ?>">VIEW ALL</a></h5>
          </div>
        </div>
      </div>
      <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9 movie-wrap">
        <div class="row movie-wrapper responsive">
          <?php foreach ($ongoing_post as $key): ?>
          <div class="element-wrap">
            <?php
              $quality = 'ongoing';
              $quality_col_right = 'col-6';
              $quality_col_left = 'col-6';
              $quality_text = 'Eps.'.$key->episode;
            ?>
            <div class="card <?php echo $quality ?>">
              <div class="hover">
                <img class="card-img-top" src="<?php echo base_url($key->url_image) ?>" alt="Card image cap">
                <div class="overlay">
                  <div class="cursor-pointer text-trailer">
                    <a onclick="show_trailer('<?php echo $key->url_trailer ?>')">Trailer</a>
                  </div>
                  <div class="text-movie">
                    <?php if ($key->jenis === 'movie'): ?>
                      <a href="<?php echo base_url('movie/'.$key->slug) ?>">Download</a>
                      <?php else: ?>
                      <a href="<?php echo base_url('series/'.$key->slug) ?>">Download</a>
                    <?php endif ?>
                  </div>
                </div>
              </div>
              <div class="text-block">
                <div class="row text-block-wrapper">
                  <div class="<?php echo $quality_col_left ?> left">
                    <img src="<?php echo base_url('assets/images/Layer-6-copy-13.png') ?>">
                    <h4><?php echo $this->Mod_home->get_movie_imdb($key->movie_imdb_id)->imdbRating; ?></h4>
                  </div>
                  <div class="<?php echo $quality_col_right ?> right-wrap">
                    <div class="right">
                      <p><?php echo $quality_text ?></p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-body">
                <p><?php echo $key->genre ?>, <?php echo explode('-', $key->release_date)[0] ?></p>
                <h5 class="card-title"><?php echo $key->title ?></h5>
              </div>
            </div>
          </div>
        <?php endforeach ?>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $(".responsive").slick({
      dots          : true,
      prevArrow     : $('.prev'),
      nextArrow     : $('.next'),
      infinite      : true,
      speed         : 300,
      slidesToShow  : 6,
      slidesToScroll: 6,
      responsive    : [
        {
          breakpoint: 1024,
          settings  : {
            slidesToShow  : 5,
            slidesToScroll: 5,
            infinite      : true,
            dots          : true
          }
        },
        {
          breakpoint: 600,
          settings  : {
            slidesToShow  : 4,
            slidesToScroll: 4
          }
        },
        {
          breakpoint: 480,
          settings  : {
            slidesToShow  : 3,
            slidesToScroll: 3
          }
        }
      ]
    });
  });
</script>