<?php $this->load->view('static/modal_trailer') ?>
<div class="ontainer-fluid  container-about-us mt-5r mw-14">
  <div class="col-12">
    <?php $this->load->view('static/horizontal_banner') ?>
  </div>
  <div class="row about-wrapper">
    <div class="col-12 col-sm-12 col-md-7 col-lg-8 col-xl-8 detail-about-wrap pl-4 pr-4">
      <h1>FAQ</h1>
      <h2>APA ITU PECINTAKOREA.COM</h2>
      <p>
        Adalah sebuah website penyedia jasa hiburan download film online yang berisikan konten-konten film Korea dan
        Asia Timur dari hasil pencarian yang ada di internet (seperti Google, Yahoo, Yandex, Bing, dan lainnya). Kami
        tidak menyimpan atau melakukan cache terhadap file film tersebut dalam server kami, kami hanya menautkan. Kami
        sangat menyarankan Anda untuk mendukung pemilik karya cipta dengan cara membeli media secara legal agar
        pemilik hak cipta dapat terus berkarya.
      </p>
      <h2>BAGAIMANA CARA UNTUK MENDOWNLOAD FILM DI PECINTAKOREA.COM?</h2>
      <p>
        Bagi Anda yang ingin mengunduh/mendownload film di website kami, Anda cukup untuk meng-klik tombol download di
        halaman page movie tersebut.
      </p>
      <h2>BAGAIMANA CARA REQUEST FILM DI PECINTAKOREA.COM?</h2>
      <p>
        Bagi Anda yang ingin merequest film, maka Anda bisa mengajukan permintaan Anda tersebut ke fanspage kami di
        Facebook.com/pecintakoreaofficial/
      </p>
      <h2>FILM TIDAK DAPAT DIPUTAR?</h2>
      <ul>
        <li>
          Koneksi internet anda kurang memadai untuk streaming. Coba anda refresh halaman streaming tersebut atau coba
          memainkan film dari lokal anda dengan cara mendownload film terlebih dahulu.
        </li>
        <li>
          Browser anda tidak mendukung untuk menampilkan video player HTML5. Kami sarankan gunakan browser Firefox
          atau Chrome dengan versi yang terbaru.
        </li>
        <li>
          Film sudah terhapus oleh provider penyimpan film. Seperti yang diketahui, film tersebut tidak disimpan di
          dalam server kami sendiri. Melainkan disimpan dari provider link tersebut masing-masing.
        </li>
      </ul>
      <h2>BAGAIMANA CARA UNTUK MELAPORKAN FILM YANG ERROR?</h2>
      <p>
        Sistem kami akan mendeteksi secara otomatis apabila film tersebut tidak dapat diputar atau error.
      </p>
    </div>
    <div class="col-md-5 col-lg-4 col-xl-4 mt-3 mb-3">
      <?php $this->load->view('static/vertical_banner') ?>
    </div>
  </div>
</div>