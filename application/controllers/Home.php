<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_home');
	}

	public function index() {
		$view = array(
			'page'				=> 'pages/home',
			'title'				=> 'Home',
			'popular_post'		=> $this->Mod_home->get_popular_post(),
			'latest_post'		=> $this->Mod_home->get_popular_post(),
			'ongoing_post'		=> $this->Mod_home->get_ongoing_post(),
			'featured_post'		=> $this->Mod_home->get_featured_post()
		);

		$this->layout->view($view);
	}

	public function redirect_home()
	{
		return redirect();
	}

	function autocomplete(){
	    if (isset($_GET['term'])) {
	    	$term = $_GET['term'];
			$specChars = array(
			    '%21' => '!',    '%22' => '"',
			    '%23' => '#',    '%24' => '$',    '%25' => '%',
			    '%26' => '&',    '%27' => '\'',   '%28' => '(',
			    '%29' => ')',    '%2A' => '*',    '%2B' => '+',
			    '%2C' => ',',    '%2D' => '-',    '%2E' => '.',
			    '%2F' => '/',    '%3A' => ':',    '%3B' => ';',
			    '%3C' => '<',    '%3D' => '=',    '%3E' => '>',
			    '%3F' => '?',    '%40' => '@',    '%5B' => '[',
			    '%5C' => '\\',   '%5D' => ']',    '%5E' => '^',
			    '%5F' => '_',    '%60' => '`',    '%7B' => '{',
			    '%7C' => '|',    '%7D' => '}',    '%7E' => '~',
			    '%E2%80%9A' => ',',  '%20' => ' '
			);

			foreach ($specChars as $k => $v) {
			    $term = str_replace($k, $v, $term);
			}
	    	$result = $this->db->select("*")->from("listmovie")->where("title like '%".$_GET['term']."%'")->limit(5)->get();
	        if ($result->num_rows() > 0) {
	           foreach ($result->result() as $row){
					$relDate=$row->release_date;
					list($relThn,$relBln,$relHri) = explode("-", $relDate);
	               $arr_result['title'] = $row->title;
	               $arr_result['image'] = $row->url_image;
	               $arr_result['genre'] = $row->genre;
	               if($row->jenis === "series")
	               {
	            		$arr_result['url'] = base_url("series/".$row->slug);
	            		if($row->progres ==="ongoing")
	            		{		            		
	            			$arr_result['desc'] = "episode ".$row->episode;
	            		}else{
	            			$arr_result['desc'] = "episode tamat";
	            		}
	       			}else{
	            		$arr_result['url'] = base_url("movie/".$row->slug);
	            		$arr_result['desc'] = "durasi ".$row->durasi;
	       			}
	               $ajax_json[] = $arr_result;
	           }
	               echo json_encode($ajax_json);
	        }
	    }
	}
}