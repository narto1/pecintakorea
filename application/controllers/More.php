<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class More extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_home');
		$this->load->model('Mod_more');
	}

	public function terpopuler($pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Terpopuler',
			'post_data'			=> $this->Mod_more->get_post_order_by($pages, 'viewer','desc'),
			'total_row'			=> $this->Mod_more->get_total_row(),
			'page_'				=> $pages,
			'page_routes'		=> 'terpopuler'
		);

		$this->layout->view($view);
	}

	public function terbaru($pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Terbaru',
			'post_data'			=> $this->Mod_more->get_post_order_by($pages, 'release_date','desc'),
			'total_row'			=> $this->Mod_more->get_total_row(),
			'page_'				=> $pages,
			'page_routes'		=> 'terbaru'
		);

		$this->layout->view($view);
	}

	public function ongoing($pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'On Going',
			'post_data'			=> $this->Mod_more->get_post_where($pages, array('progres'=>'ongoing', 'jenis'=>'series')),
			'total_row'			=> $this->Mod_more->get_total_row(array('jenis'=>'series')),
			'page_'				=> $pages,
			'page_routes'		=> 'ongoing'
		);

		$this->layout->view($view);
	}

	public function genre($genre, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Genre '.$genre,
			'post_data'			=> $this->Mod_more->get_post_like($pages, array('genre'=>$genre)),
			'total_row'			=> $this->Mod_more->get_total_row_like(array('genre'=>$genre)),
			'page_'				=> $pages,
			'page_routes'		=> 'genre/'.$genre
		);

		$this->layout->view($view);
	}

	public function actor($actor, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$actor = $this->Mod_more->correct_keyword($actor);
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Actor '.$actor,
			'post_data'			=> $this->Mod_more->get_post_like($pages, array('actor'=>$actor)),
			'total_row'			=> $this->Mod_more->get_total_row_like(array('actor'=>$actor)),
			'page_'				=> $pages,
			'page_routes'		=> 'actor/'.$actor
		);

		$this->layout->view($view);
	}

	public function production($production, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$production = $this->Mod_more->correct_keyword($production);
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Production '.$production,
			'post_data'			=> $this->Mod_more->get_post_like($pages, array('produksi'=>$production)),
			'total_row'			=> $this->Mod_more->get_total_row_like(array('produksi'=>$production)),
			'page_'				=> $pages,
			'page_routes'		=> 'production/'.$production
		);

		$this->layout->view($view);
	}

	public function director($director, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$director = $this->Mod_more->correct_keyword($director);
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Director '.$director,
			'post_data'			=> $this->Mod_more->get_post_like($pages, array('director'=>$director)),
			'total_row'			=> $this->Mod_more->get_total_row_like(array('director'=>$director)),
			'page_'				=> $pages,
			'page_routes'		=> 'director/'.$director
		);

		$this->layout->view($view);
	}

	public function country($country, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$country = $this->Mod_more->correct_keyword($country);
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Country '.$country,
			'post_data'			=> $this->Mod_more->get_post_like($pages, array('country'=>$country)),
			'total_row'			=> $this->Mod_more->get_total_row_like(array('country'=>$country)),
			'page_'				=> $pages,
			'page_routes'		=> 'country/'.$country
		);

		$this->layout->view($view);
	}

	public function list($list, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$list = $this->Mod_more->correct_keyword($list);
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'List '.$list,
			'post_data'			=> $this->Mod_more->get_post_order_by_where($pages, 'title', 'asc', array('title LIKE '=>$list.'%')),
			'total_row'			=> $this->Mod_more->get_total_row(array('title LIKE '=>$list.'%')),
			'page_'				=> $pages,
			'page_routes'		=> 'list/'.$list
		);

		$this->layout->view($view);
	}

	public function search($title, $pages = 1) {
		if (empty($pages)) {
			$pages = 1;
		}
		$search = $this->Mod_more->correct_keyword($title);
		$view = array(
			'page'				=> 'pages/more',
			'title'				=> 'Search '.$search,
			'post_data'			=> $this->Mod_more->get_post_like($pages, array('title'=>$title)),
			'total_row'			=> $this->Mod_more->get_total_row_like(array('title'=>$title)),
			'page_'				=> $pages,
			'page_routes'		=> 'search/'.$search
		);

		$this->layout->view($view);
	}

	public function search_keyword()
	{
		return redirect('search/'.$this->input->post('post_judul').'/');
	}
}