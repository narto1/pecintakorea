<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Single extends Panel_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('layout');
		$this->load->model('Mod_single');
		$this->load->model('Mod_home');
	}

	public function movie($slug = null) {
		if ($slug === null) {
			return redirect('home');
		}else{
			$post_data = $this->Mod_single->get_movie_data(array('slug'=>$slug));
			$this->Mod_single->check_movie($slug);
		}
		$view = array(
			'page'				=> 'pages/single_movie',
			'title'				=> $post_data->title,
			'movie_data'		=> $this->Mod_single->get_movie_data(array('slug'=>$slug)),
			'related_post'		=> $this->Mod_single->get_related_post($slug),
			'big_four'			=> $this->Mod_home->get_big_four(),
			'popular_post'		=> $this->Mod_home->get_popular_post(),
			'latest_post'		=> $this->Mod_home->get_popular_post(),
			'ongoing_post'		=> $this->Mod_home->get_ongoing_post(),
			'featured_post'		=> $this->Mod_home->get_featured_post(),
			'slug'				=> $slug,
			'meta_keyword'		=> $post_data->tag
		);
		$this->Mod_single->add_viewer($slug);
		$this->layout->view($view);
	}

	public function series($slug = null, $episode = null) {
		if ($episode === null) {
			$episode = 1;
		}
		if ($slug === null) {
			return redirect('home');
		}else{
			$post_data = $this->Mod_single->get_movie_data(array('slug'=>$slug));
			$this->Mod_single->check_series($slug, $episode, $post_data->movie_imdb_id);
		}
		$view = array(
			'page'				=> 'pages/single_series',
			'title'				=> $post_data->title,
			'movie_data'		=> $this->Mod_single->get_movie_data(array('slug'=>$slug)),
			'episode_data'		=> $this->Mod_single->get_episode_data(array('movie_imdb_id'=>$post_data->movie_imdb_id, 'episode'=>$episode)),
			'all_episode'		=> $this->Mod_single->get_all_episode(array('movie_imdb_id'=>$post_data->movie_imdb_id)),
			'related_post'		=> $this->Mod_single->get_related_post($slug),
			'big_four'			=> $this->Mod_home->get_big_four(),
			'popular_post'		=> $this->Mod_home->get_popular_post(),
			'latest_post'		=> $this->Mod_home->get_popular_post(),
			'ongoing_post'		=> $this->Mod_home->get_ongoing_post(),
			'featured_post'		=> $this->Mod_home->get_featured_post(),
			'slug'				=> $slug,
			'meta_keyword'		=> $post_data->tag
		);
		$this->Mod_single->add_viewer($slug);
		$this->layout->view($view);
	}

	public function request() {
		$this->form_validation->set_rules('post_email', 'Email', 'required');
		$this->form_validation->set_rules('post_judul', 'Judul', 'required');
		if($this->form_validation->run() == FALSE)
		{
			$view = array(
				'page'				=> 'pages/single_request',
				'title'				=> 'Request Film'
			);
			$this->layout->view($view);
		}else{
			return $this->Mod_single->send_request_film($this->input->post('post_email'), $this->input->post('post_judul'));
		}
	}

	public function dmca() {
		$view = array(
			'page'				=> 'pages/single_dmca',
			'title'				=> 'DMCA'
		);
		$this->layout->view($view);
	}

	public function faq() {
		$view = array(
			'page'				=> 'pages/single_faq',
			'title'				=> 'FAQ'
		);
		$this->layout->view($view);
	}
}